Description
===========

A simple test suite for the representation git submodule which should be used from different project at ESRF.


Installation
============

.. code-block:: bash

    git clone –recursive https://gitlab.esrf.fr/workflow/representationtest.git
    pip install .[full]
