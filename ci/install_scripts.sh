
function install_est(){
      unset http_proxy
      unset https_proxy
      git clone https://gitlab.esrf.fr/workflow/est.git --single-branch --branch $1
      cd est
      pip install -r requirements-dev.txt
      pip install pymca
      pip install .
      export http_proxy=http://proxy.esrf.fr:3128/
      export https_proxy=http://proxy.esrf.fr:3128/
      cd ..
}
