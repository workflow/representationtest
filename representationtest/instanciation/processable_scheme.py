from ..representation import Scheme
from .workflow import ActorFactory, StartActor, StopActor, JoinUntilStopSignal


class ProcessableScheme(object):
    def __init__(self, scheme):
        assert isinstance(scheme, Scheme)
        self._representation = scheme
        # first load node handlers if any
        scheme.load_handlers()

        self._actor_factory = {}
        for node in self._representation.nodes:
            self._actor_factory[node] = ActorFactory(node)

        # deal with connect
        for node in self._representation.nodes:
            actor_factory = self._actor_factory[node]
            for downstream_node in node.downstream_nodes:
                downstream_actor_factory = self._actor_factory[downstream_node]
                actor_factory.connect(downstream_actor_factory)

        # add start actor
        self._start_actor = StartActor()
        for node in self._representation.start_nodes():
            actor_factory = self._actor_factory[node]
            self._start_actor.connect(actor_factory)

        def connect_finals_nodes(actor):
            # add end actor
            for node in self._representation.final_nodes():
                actor_factory = self._actor_factory[node]
                actor_factory.connect(actor)

        self._end_actor = StopActor()

        if self.has_final_join():
            self._join_actor = JoinUntilStopSignal("stop join")
            connect_finals_nodes(self._join_actor)
            self._join_actor.connect(self._end_actor)
        else:
            connect_finals_nodes(self._end_actor)

    def has_final_join(self):
        """True if we need to send a 'end' signal before closing the workflow
        This is needed for DataList and DataWatcher
        """
        for node in self._representation.nodes:
            if node.need_stop_join:
                return True
        return False
