from ..representation.scheme.node import Node, WorkflowException
import multiprocessing
import logging

logging.basicConfig(level=logging.INFO, format="%(asctime)s %(message)s")
logger = logging.getLogger()


def exec_process(name, input_, properties):
    logger.debug(
        " ".join(
            (
                "processing",
                str(name),
                "with input",
                str(input_),
                "and",
                str(properties),
                "as properties",
            )
        )
    )
    if type(input_) is tuple:
        data_name, data = input_
    else:
        data_name = None
        data = input_
    return Node.execute(
        process_pt=name, properties=properties, input_data=data, input_name=data_name
    )


class AsyncFactory:
    def __init__(self, actor, callback=None):
        self.actor = actor
        self.pool = multiprocessing.Pool()
        self.callback = callback

    def call(self, *args, **kwargs):
        logger.debug("args={0}, kwargs={1}".format(args, kwargs))
        if self.callback is None:
            self.pool.apply_async(exec_process, args, kwargs)
        else:
            self.pool.apply_async(exec_process, args, kwargs, self.callback)


class ActorWrapper(object):
    def __init__(self, actor):
        # assert isinstance(actor, Node)
        self.actor = actor

    def run(self, inData):
        outData = self.actor.run(inData)
        if isinstance(outData, WorkflowException):
            return outData
        else:
            inData.update(outData)
            return inData


class ActorFactory(object):
    def __init__(self, name, errorHandler=None):
        self.name = name
        self.errorHandler = errorHandler
        self._a = None
        self.listDownStreamActor = []
        self.outData = None
        self.actorWrapper = ActorWrapper(name)
        self.af = None
        self.lock = multiprocessing.Lock()

    def connect(self, actor):
        self.listDownStreamActor.append(actor)

    def _trigger_process(self, inData):
        """Trigger one process"""
        logger.debug("In my trigger {0}, inData = {1}".format(self.name, inData))
        if isinstance(inData, WorkflowException):
            logger.error(
                "Error from previous actor! Not running actor {0}".format(self.name)
            )
            if self.errorHandler is not None:
                workflowException = inData
                oldInData = workflowException.data
                exceptionDict = {
                    "errorMessage": workflowException.errorMessage,
                    "traceBack": workflowException.traceBack,
                }
                oldInData["WorkflowException"] = exceptionDict
                self.errorHandler.triggerOnError(oldInData)
        elif len(self.listDownStreamActor) == 0:
            # Run without any callback
            self.af = AsyncFactory(self.actorWrapper.actor.process_pt)
            self.af.call(
                self.actorWrapper.actor._processing_pt,
                inData,
                self.actorWrapper.actor.properties,
            )
        else:
            # Run with callback
            self.af = AsyncFactory(
                self.actorWrapper.actor.process_pt, self.triggerDownStreamActor
            )
            self.af.call(
                self.actorWrapper.actor.process_pt,
                inData,
                self.actorWrapper.actor.properties,
            )

    def trigger(self, data):
        if hasattr(data, "to_dict"):
            _data = data.to_dict()
        else:
            _data = data
        if _data and "sig_type" in _data:
            self._trigger_signal(data=_data)
        else:
            properties = self.actorWrapper.actor.properties
            if _data is None and "_scanIDs" in properties:
                inDataList = properties["_scanIDs"]
            else:
                inDataList = [
                    _data,
                ]

            for inData in inDataList:
                self._trigger_process(inData=inData)

            if self.actorWrapper.actor.need_stop_join:
                self._trigger_stop(len(inDataList))

    def _trigger_stop(self, nProcess):
        """Signal that this process is finished and the downstream nodes
        Can stop once they processed nProcess"""
        self._trigger_signal(data={"sig_type": "stop", "n_process": nProcess})

    def _trigger_signal(self, data):
        if len(self.listDownStreamActor) == 0:
            return
        else:
            self.triggerDownStreamActor(inData=data)

    def triggerDownStreamActor(self, inData=None):
        if inData is None:
            inData = {}
        for downStreamActor in self.listDownStreamActor:
            logger.debug(
                "In trigger {0}, triggering actor {1}, inData={2}".format(
                    self.name, downStreamActor.name, inData
                )
            )
            downStreamActor.trigger(inData)


class StartActor(object):
    def __init__(self, name="Start actor"):
        self.name = name
        self.listDownStreamActor = []

    def connect(self, actor):
        self.listDownStreamActor.append(actor)

    def trigger(self, inData):
        for actor in self.listDownStreamActor:
            actor.trigger(inData)


class StopActor(object):
    def __init__(self, name="Stop actor"):
        self.name = name
        self.lock = multiprocessing.Lock()
        self.lock.acquire()
        self.outData = None

    def trigger(self, inData):
        logger.debug("In trigger {0}, inData = {1}".format(self.name, inData))
        self.outData = inData
        self.lock.release()

    def join(self, timeout=7200):
        self.lock.acquire(timeout=timeout)


class Join(object):
    def __init__(self, name, numberOfConnections):
        self.name = name
        self.numberOfConnections = numberOfConnections
        self.listInData = []
        self.listDownStreamActor = []

    def connect(self, actor):
        self.listDownStreamActor.append(actor)

    def trigger(self, inData):
        self.listInData.append(inData)
        if len(self.listInData) == self.numberOfConnections:
            newInData = {}
            for data in self.listInData:
                newInData.update(data)
            for actor in self.listDownStreamActor:
                actor.trigger(newInData)


class JoinUntilStopSignal(object):
    def __init__(self, name):
        self.name = name
        self.listInData = []
        self.listDownStreamActor = []
        self._nprocess_received = 0
        self._nprocess_waited = 0
        self._can_stop = False

    def connect(self, actor):
        self.listDownStreamActor.append(actor)

    def trigger(self, inData):
        if (
            type(inData) is dict
            and "sig_type" in inData
            and inData["sig_type"] == "stop"
        ):
            self._can_stop = True
            self._nprocess_waited = inData["n_process"]
        else:
            self._nprocess_received += 1

        self.listInData.append(inData)
        if self._can_stop and self._nprocess_waited <= self._nprocess_received:
            newInData = {}
            for data in self.listInData:
                newInData.update(data)
            for actor in self.listDownStreamActor:
                actor.trigger(newInData)


class RouterActor(object):
    def __init__(self, parent=None, name="Router", itemName=None, listPort=[]):
        self.parent = parent
        self.name = name
        self.itemName = itemName
        self.listPort = listPort
        self.dictValues = {}

    def connect(self, actor, expectedValue="other"):
        if expectedValue != "other" and not expectedValue in self.listPort:
            raise RuntimeError(
                "Port {0} not defined for router actor {1}!".format(
                    expectedValue, self.name
                )
            )
        self.dictValues[expectedValue] = actor

    def trigger(self, inData):
        value = inData[self.itemName]
        if value in self.dictValues:
            self.dictValues[value].trigger(inData=inData)
        else:
            self.dictValues["other"].trigger(inData=inData)


class Port(object):
    def __init__(self, parent, name):
        self.name = parent.name + "." + name
        self.parent = parent
        self.listActor = []
        self.inPortTrigger = None

    def connect(self, actor):
        logger.debug("Connect {0} -> actorName {1}".format(self.name, actor.name))
        self.listActor.append(actor)

    def setTrigger(self, trigger):
        self.inPortTrigger = trigger

    def trigger(self, inData):
        logger.debug("In {0} trigger".format(self.name))
        if len(self.listActor) > 0:
            for actor in self.listActor:
                logger.debug(
                    "In trigger {0} -> actorName {1}".format(
                        self.parent.name, actor.name
                    )
                )
                actor.trigger(inData)
        if self.inPortTrigger is not None:
            logger.debug(
                "In {0} trigger, trigger = {1}".format(
                    self.parent.name, self.inPortTrigger
                )
            )
            self.inPortTrigger(inData)


class SubModel(object):
    def __init__(self, errorHandler=None, name=None, portNames=["In", "Out"]):
        self.name = name
        self.errorHandler = errorHandler
        self.dictPort = {}
        self.listOnErrorActor = []
        for portName in portNames:
            self.dictPort[portName] = Port(self, portName)

    def getPort(self, portName):
        logger.debug("In {0} getPort, portName = {1}".format(self.name, portName))
        return self.dictPort[portName]

    def connect(self, actor, portName="Out"):
        logger.debug(
            "In {0} connect, portName = {2} -> actorName = {1}".format(
                self.name, actor.name, portName
            )
        )
        self.dictPort[portName].connect(actor)

    def connectOnError(self, actor):
        logger.debug(
            "In connectOnError in subModule {0}, actor name {1}".format(
                self.name, actor.name
            )
        )
        self.listOnErrorActor.append(actor)

    def triggerOnError(self, inData):
        for onErrorActor in self.listOnErrorActor:
            logger.debug(
                "In triggerOnError in subModule {0}, trigger actor {1}, inData = {2}".format(
                    self.name, onErrorActor.name, inData
                )
            )
            onErrorActor.trigger(inData)
        if self.errorHandler is not None:
            self.errorHandler.triggerOnError(inData)


class ErrorHandler(object):
    def __init__(self, name="Start actor"):
        self.name = name
        self.listDownStreamActor = []

    def connect(self, actor):
        self.listDownStreamActor.append(actor)

    def triggerOnError(self, inData):
        for actor in self.listDownStreamActor:
            actor.trigger(inData)
