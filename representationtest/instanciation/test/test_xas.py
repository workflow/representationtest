# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2015-2019 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "19/08/2019"


import unittest
import os
from representationtest.resources import resource_filename
from representationtest.representation.scheme.ows_parser import OwsParser

try:
    import xas
    import PyMca5
except ImportError:
    _has_xas_wth_pymca = False
else:
    _has_xas_wth_pymca = True


@unittest.skipUnless(_has_xas_wth_pymca, "xas and/or pymca not installed")
class TestXASWorkflow(unittest.TestCase):
    def setUp(self):
        self.xas_ows_file = resource_filename("owsfiles/example_xas.ows")
        assert os.path.isfile(self.xas_ows_file)

    def testLoad(self):
        """Test that the parser can correctly load the ows file from xas.
        use a specific bramch of xas to be sure of the compatibility"""
        scheme = OwsParser.scheme_load(file_=self.xas_ows_file, load_handlers=True)
        # check handlers
        xas_input_node = scheme.nodes[0]
        exafs_node = scheme.nodes[3]
        kweight_node = scheme.nodes[4]


def suite():
    test_suite = unittest.TestSuite()
    for ui in (TestXASWorkflow,):
        test_suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite


if __name__ == "__main__":
    unittest.main(defaultTest="suite")
