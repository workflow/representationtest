# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2015-2019 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "19/08/2019"


import unittest
from representationtest.representation import Scheme, Node, Link
from representationtest.instanciation.processable_scheme import ProcessableScheme
from representationtest.instanciation.main import exec_


def mycallback(input_):
    """Simple callback function for test"""
    if input_:
        input_.update({"called": True})
        if not isinstance(input_, dict):
            raise ValueError("invalid input, dict requested")
    else:
        return {"called": True}
    return input_


class MyCallable(object):
    """Simple callable class"""

    def __call__(self, input_):
        return {"called": True}


class MyObject(object):
    """Simple object with handler"""

    inputs = [("mydata", dict, "handler_1"), ("mydata2", dict, "handler_2")]

    def handler_1(self, input_):
        input_.update({"called": True})
        return ("mydata2", input_)

    def handler_2(self, input_):
        input_.update({"also called": True})
        return input_

    def process(self, input_=None):
        return ("mydata", {"started": True})


class TestHandlers(unittest.TestCase):
    """Make sure the process are calling the correct handler according to the
    input channel"""

    def test_nodes_fct_callback(self):
        """simple scheme with two nodes. Callback is a function"""
        node = Node(processing_pt=mycallback)
        node.load_handlers()
        self.assertEqual(len(node.handlers), 1)
        self.assertTrue(node.handlers[None] == mycallback)
        my_scheme = Scheme(nodes=[node])
        processable_scheme = ProcessableScheme(scheme=my_scheme)
        out_ = exec_(scheme=processable_scheme)
        self.assertEqual((None, {"called": True}), out_)

    def test_nodes_callable_obj(self):
        """simple scheme with two nodes. Callback is a callable object"""
        node = Node(processing_pt=".".join((__file__, "MyCallable")))
        node.load_handlers()
        self.assertEqual(len(node.handlers), 1)
        my_scheme = Scheme(nodes=[node])
        processable_scheme = ProcessableScheme(scheme=my_scheme)
        out_ = exec_(scheme=processable_scheme)
        self.assertEqual((None, {"called": True}), out_)

    # def test_nodes_obj_handler_function(self):
    #     """simple scheme with two nodes. Callback is a specific function of an
    #     object to instantiate
    #     """
    #     node1 = Node(processing_pt='.'.join((__file__, 'MyObject')))
    #     node2 = Node(processing_pt='.'.join((__file__, 'MyObject')))
    #     node3 = Node(processing_pt='.'.join((__file__, 'MyObject')))
    #     node1.load_handlers()
    #     node2.load_handlers()
    #     node3.load_handlers()
    #
    #     link1 = Link(node1, node2, 'mydata', 'mydata')
    #     link2 = Link(node2, node3, 'mydata2', 'mydata2')
    #
    #     nodes = [node1, node2, node3]
    #     links = [link1, link2]
    #
    #     my_scheme = Scheme(nodes=nodes, links=links)
    #     # with TestLogging(obj_logger, critical=1, warning=1, info=1):
    #     processable_scheme = ProcessableScheme(scheme=my_scheme)
    #     out_ = exec_(scheme=processable_scheme)
    #     self.assertTrue('started' in out_)
    #     self.assertTrue('called' in out_)
    #     self.assertTrue('also called' in out_)


def suite():
    test_suite = unittest.TestSuite()
    for ui in (TestHandlers,):
        test_suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite


if __name__ == "__main__":
    unittest.main(defaultTest="suite")
