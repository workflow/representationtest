# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2015-2019 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "19/08/2019"


import unittest
import tempfile
import shutil
import json
import os
from representationtest.representation import Node, Link, Scheme


class TestSaveReadJson(unittest.TestCase):
    """Test Json io"""

    def setUp(self):
        self._tmp_dir = tempfile.mkdtemp()
        self.json_file_path = os.path.join(self._tmp_dir, "myfile.json")

        self.node1 = Node(processing_pt="print")
        self.node2 = Node(processing_pt="print")
        self.link = Link(self.node1, self.node2, "mystring", "mystring")

        self.scheme = Scheme(nodes=(self.node1, self.node2), links=(self.link,))
        self.scheme.description = "my description"
        self.scheme.title = "my title"

    def tearDown(self):
        shutil.rmtree(self._tmp_dir)

    def save(self, scheme):
        scheme.save_as_json(self.json_file_path)

    def testSave(self):
        """save the json and check quickly if the json file seems valid"""
        self.assertFalse(os.path.exists(self.json_file_path))
        self.save(self.scheme)
        self.assertTrue(os.path.exists(self.json_file_path))
        # TODO: test that some nodes exists
        with open(self.json_file_path) as json_file:
            json_data = json.load(json_file)
        self.assertTrue(Scheme._JSON_TITLE in json_data)
        self.assertTrue(Scheme._JSON_DESCRIPTION in json_data)
        self.assertTrue(Scheme._JSON_LINKS in json_data)
        self.assertTrue(Scheme._JSON_NODES in json_data)
        self.assertTrue(len(json_data[Scheme._JSON_NODES]) == 2)
        self.assertTrue(len(json_data[Scheme._JSON_LINKS]) == 1)

    def testRead(self):
        """
        Save the scheme to json then read it and make sure not main information
        is lost between those process
        """
        self.save(self.scheme)
        self.assertTrue(os.path.exists(self.json_file_path))
        new_scheme = Scheme.from_json_file(self.json_file_path)
        # check scheme metadata
        self.assertTrue(new_scheme.description == self.scheme.description)
        self.assertTrue(new_scheme.title == self.scheme.title)
        # check nodes
        original_scheme_nodes_ids = self.scheme.nodes_dict.keys()
        new_scheme_nodes_ids = new_scheme.nodes_dict.keys()
        self.assertTrue(original_scheme_nodes_ids == new_scheme_nodes_ids)
        for node_id in original_scheme_nodes_ids:
            self.assertTrue(
                self.scheme.nodes_dict[node_id].process_pt
                == new_scheme.nodes_dict[node_id].process_pt
            )
        # check links
        self.assertTrue(new_scheme.links.keys() == self.scheme.links.keys())
        for ori_link in self.scheme.links.values():
            new_link = new_scheme.links[ori_link.id]
            self.assertTrue(ori_link.source_node_id == new_link.source_node_id)
            self.assertTrue(ori_link.sink_node_id == new_link.sink_node_id)
            self.assertTrue(ori_link.source_channel == new_link.source_channel)
            self.assertTrue(ori_link.sink_channel == new_link.sink_channel)


def suite():
    test_suite = unittest.TestSuite()
    for ui in (TestSaveReadJson,):
        test_suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite


if __name__ == "__main__":
    unittest.main(defaultTest="suite")
