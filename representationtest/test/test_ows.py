# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2015-2019 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "19/08/2019"


import unittest
import os
from representationtest.resources import resource_filename
from representationtest.representation.scheme.ows_parser import OwsParser
from representationtest.representation import Node

try:
    import xas
    import pymca5
except ImportError:
    _has_xas_wth_pymca = False
else:
    _has_xas_wth_pymca = True


class TestXASWorkflow(unittest.TestCase):
    def setUp(self):
        self.xas_ows_file = resource_filename("owsfiles/example_xas.ows")
        assert os.path.isfile(self.xas_ows_file)

    def testLoad(self):
        """Test that the parser can correctly load the ows file from xas"""
        scheme = OwsParser.scheme_load(file_=self.xas_ows_file, load_handlers=False)
        self.assertTrue(scheme is not None)
        self.assertEqual(len(scheme.nodes), 6)
        self.assertEqual(len(scheme.links), 5)
        self.assertEqual(len(scheme.final_nodes()), 1)
        self.assertEqual(len(scheme.start_nodes()), 1)
        xas_input_node = scheme.nodes[0]
        exafs_node = scheme.nodes[3]
        kweight_node = scheme.nodes[4]
        assert isinstance(xas_input_node, Node)

        process_pt = "orangecontrib.xas.widgets.utils.xas_input.XASInputOW"
        aliases = OwsParser.get_aliases()
        # the _process_pt can have two value, the original value if xas is not
        # installed or installed with a version not defining the pypushflowaddon
        if process_pt in aliases:
            process_pt = aliases[process_pt]

        # original value is
        self.assertTrue(xas_input_node._process_pt == process_pt)
        self.assertEqual(len(xas_input_node.upstream_nodes), 0)
        self.assertEqual(len(xas_input_node.downstream_nodes), 1)
        self.assertEqual(len(exafs_node.upstream_nodes), 1)
        self.assertEqual(len(exafs_node.downstream_nodes), 1)
        self.assertTrue(exafs_node.downstream_nodes == set([kweight_node,]))


class TestTomwerWorkflow(unittest.TestCase):
    def setUp(self):
        self.tomwer_ows_file = resource_filename("owsfiles/example_tomwer.ows")
        assert os.path.isfile(self.tomwer_ows_file)

    def testLoad(self):
        """Test that the parser can correctly load the ows file from tomwer"""
        scheme = OwsParser.scheme_load(file_=self.tomwer_ows_file, load_handlers=False)
        self.assertTrue(scheme is not None)
        self.assertEqual(len(scheme.nodes), 6)
        self.assertEqual(len(scheme.links), 6)
        self.assertEqual(len(scheme.final_nodes()), 1)
        self.assertEqual(len(scheme.start_nodes()), 1)
        data_watcher_node = scheme.nodes[0]
        dark_ff_node = scheme.nodes[4]
        ftseries_node = scheme.nodes[2]
        scanvalidator_node = scheme.nodes[3]
        self.assertTrue(
            data_watcher_node._process_pt
            == "orangecontrib.tomwer.widgets.control.TomoDirOW.TomoDirOW"
        )
        self.assertTrue(
            scanvalidator_node._process_pt
            == "orangecontrib.tomwer.widgets.control.DataValidatorOW.DataValidatorOW"
        )
        self.assertTrue(data_watcher_node.downstream_nodes == set([dark_ff_node,]))
        self.assertEqual(len(scanvalidator_node.downstream_nodes), 2)
        self.assertEqual(len(ftseries_node.upstream_nodes), 2)


def suite():
    test_suite = unittest.TestSuite()
    for ui in (
        TestXASWorkflow,
        TestTomwerWorkflow,
    ):
        test_suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite


if __name__ == "__main__":
    unittest.main(defaultTest="suite")
