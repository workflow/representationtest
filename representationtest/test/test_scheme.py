# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2015-2019 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "19/08/2019"


import unittest
from representationtest.representation import Scheme, Node, Link, SubScheme
from representationtest.representation.scheme.node import ErrorHandler
import tempfile
import shutil
import os


class TestScheme(unittest.TestCase):
    """Test the Scheme class"""

    def testSchemeDefinition(self):
        """simple test of scheme definition"""
        node1 = Node(processing_pt="print")
        node2 = Node(processing_pt="print")
        link = Link(node1, node2, "mystring", "mystring")

        scheme = Scheme(nodes=(node1, node2), links=(link,))
        self.assertEqual(len(scheme.nodes), 2)
        self.assertEqual(len(scheme.links), 1)
        self.assertTrue(
            scheme.start_nodes() == [node1,]
        )
        self.assertTrue(
            scheme.final_nodes() == [node2,]
        )


class TestSubScheme(unittest.TestCase):
    """Test the SubScheme workflow definition"""

    def setUp(self) -> None:
        self.scheme = self.createWorkflowDefinition()
        self._working_folder = tempfile.mkdtemp()

    def tearDown(self) -> None:
        shutil.rmtree(self._working_folder)

    def createCommonErrorReporterSubScheme(self):
        self.in_port = Node(processing_pt="SubModuleNodeEntry")
        self.out_port = Node(processing_pt="SubModuleNodeEntry")
        self.logger_report_error_node = Node(
            processing_pt="mx.src.logger_report_error.py"
        )
        self.ispyb_set_status_failure_node = Node(
            processing_pt="mx.src.ispyb_set_status_failure.py"
        )
        self.display_error_msg_node = Node(
            processing_pt="mx.src.displayErrorMessage.py"
        )
        self.link0 = Link(
            source_node=self.in_port, sink_node=self.logger_report_error_node
        )
        self.link1 = Link(
            source_node=self.logger_report_error_node,
            sink_node=self.ispyb_set_status_failure_node,
        )
        self.link2 = Link(
            source_node=self.ispyb_set_status_failure_node,
            sink_node=self.display_error_msg_node,
        )
        self.link3 = Link(
            source_node=self.display_error_msg_node, sink_node=self.out_port
        )
        self.sub_nodes = [
            self.in_port,
            self.logger_report_error_node,
            self.ispyb_set_status_failure_node,
            self.display_error_msg_node,
            self.out_port,
        ]
        self.sub_links = [self.link0, self.link1, self.link2, self.link3]
        return SubScheme(
            description="submodule", nodes=self.sub_nodes, links=self.sub_links
        )

    def createWorkflowDefinition(self):
        """Test the subscheme workflow definition"""
        self.check_move_of_phi = Node(processing_pt="mx.src.checkMoveOfPhi.py")

        self.sub_scheme = self.createCommonErrorReporterSubScheme()

        self.link_input = Link(
            source_node=self.check_move_of_phi,
            sink_node=self.sub_scheme,
            source_channel="In",
            sink_channel="In",
        )

        self.errorHandler = ErrorHandler(processing_pt="errorhandler.py")

        self.ispyb_set_status_success_with_errors_node = Node(
            processing_pt="mx.src.ispyb_set_status_success_with_errors.py",
            error_handler=self.errorHandler,
        )

        self.link_output = Link(
            source_node=self.sub_scheme,
            sink_node=self.ispyb_set_status_success_with_errors_node,
            source_channel="Out",
            sink_channel="Out",
        )

        self.nodes = (
            self.check_move_of_phi,
            self.ispyb_set_status_success_with_errors_node,
            self.sub_scheme,
        )
        self.links = (self.link_input, self.link_output)
        return Scheme(nodes=self.nodes, links=self.links)

    def check_scheme_validity(self, scheme):
        """Test that scheme is valid regarding self.scheme theoretical
        configuration"""
        self.assertEqual(len(scheme.nodes), 3)
        self.assertEqual(len(scheme.rnodes), 8)
        self.assertEqual(len(scheme.links), 2)
        self.assertEqual(len(scheme.rlinks), 6)
        self.assertEqual(len(scheme.sub_schemes), 1)
        self.assertEqual(len(scheme.rsub_schemes), 1)
        self.assertEqual(len(scheme.sub_schemes), 1)
        # test the sub scheme
        sub_scheme = scheme.sub_schemes[0]
        self.assertEqual(len(sub_scheme.nodes), 5)
        self.assertEqual(len(sub_scheme.rnodes), 5)
        self.assertEqual(len(sub_scheme.links), 4)
        self.assertEqual(len(sub_scheme.rlinks), 4)
        self.assertEqual(len(sub_scheme.rsub_schemes), 0)

    def testSchemeCreation(self):
        """Make sure the scheme is working and valid"""
        # test the root scheme
        self.check_scheme_validity(scheme=self.scheme)

    def testJsonCreation(self):
        """Test the json write is working and we can read back the Scheme"""
        file_path = os.path.join(self._working_folder, "output_test.json")
        self.scheme.save_to(file_path)
        self.assertTrue(os.path.exists(file_path))
        scheme_loaded = Scheme.from_json_file(json_file_path=file_path)
        self.assertTrue(scheme_loaded is not None)
        # should contain all nodes + the submodules
        self.check_scheme_validity(scheme=scheme_loaded)

    def testExecution(self):
        pass


def suite():
    test_suite = unittest.TestSuite()
    for ui in (TestScheme, TestSubScheme):
        test_suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite


if __name__ == "__main__":
    unittest.main(defaultTest="suite")
