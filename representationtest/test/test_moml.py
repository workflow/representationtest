# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2015-2019 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "19/08/2019"


import unittest
import os
from representationtest.resources import resource_filename
from representationtest.representation.scheme.moml_parser import MomlParser
from representationtest.representation import Node

try:
    import xas
    import pymca5
except ImportError:
    _has_xas_wth_pymca = False
else:
    _has_xas_wth_pymca = True


class TestMomlWorkflow(unittest.TestCase):
    def testLoadPrepareMesh(self):
        """Test that the parser can correctly load some moml files"""
        moml_file = resource_filename("momlfiles/PrepareMesh.moml")
        assert os.path.isfile(moml_file)
        scheme = MomlParser.scheme_load(file_=moml_file, load_handlers=False)
        self.assertTrue(scheme is not None)
        # TODO: In, Out and "No mesh defined" are not managed yet
        self.assertEqual(len(scheme.nodes), 4)
        self.assertEqual(len(scheme.links), 2)
        self.assertEqual(len(scheme.final_nodes()), 2)
        self.assertEqual(len(scheme.start_nodes()), 2)


def suite():
    test_suite = unittest.TestSuite()
    for ui in (TestMomlWorkflow,):
        test_suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite
